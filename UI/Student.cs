﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication6.BAL;
using WindowsFormsApplication6.Model;

namespace WindowsFormsApplication6.UI
{
    public partial class Student : Form
    {
        private DepartmentBAL dptbal = new DepartmentBAL();
        private StudentBAL stdbal = new StudentBAL();
        public Student()
        {
            InitializeComponent();
        }

        private void Student_Load(object sender, EventArgs e)
        {
            PopulateDepartment();
            PopulateStudent();
        }

        private void PopulateStudent()
        {
            List<StudentModel> stdList = stdbal.GetAllStudent();
            studentListView.Items.Clear();
            foreach (StudentModel std in stdList) { 
                ListViewItem items = new ListViewItem();
                items.Text = std.Id.ToString();
                items.SubItems.Add(std.Name);
                items.SubItems.Add(std.PhoneNo);
                items.SubItems.Add(std.Email);
                items.SubItems.Add(std.RegNo);
                items.SubItems.Add(std.Department.ToString());
                items.Tag = std;

                studentListView.Items.Add(items);

            }

        }

        private void PopulateDepartment()
        {
            List<DepartmentModel> deptList = dptbal.GetAllDepartment();
            departmentcomboBox.DataSource = deptList;
            departmentcomboBox.DisplayMember = "Name";
            departmentcomboBox.ValueMember = "Id";

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            StudentModel std = new StudentModel();
            std.Name = nametext.Text.Trim();
            std.PhoneNo = phonetext.Text.Trim();
            std.Email = emailtext.Text.Trim();
            std.RegNo = regtext.Text.Trim();
            std.Department = Convert.ToInt32(departmentcomboBox.SelectedValue);

            string message = stdbal.SaveStudent(std);
            MessageBox.Show(message);
            PopulateStudent();
        }

        private void studentListView_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem selectedItem = studentListView.SelectedItems[0];
            StudentModel std = (StudentModel)selectedItem.Tag;
            nametext.Text = std.Name;
            phonetext.Text = std.PhoneNo;
            emailtext.Text = std.Email;
            regtext.Text = std.RegNo;
            departmentcomboBox.SelectedValue = std.Department;
        }
    }
}
