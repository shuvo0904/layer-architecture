﻿namespace WindowsFormsApplication6.UI
{
    partial class Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.phonetxt = new System.Windows.Forms.Label();
            this.emailtxt = new System.Windows.Forms.Label();
            this.regtxt = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.nametext = new System.Windows.Forms.TextBox();
            this.phonetext = new System.Windows.Forms.TextBox();
            this.emailtext = new System.Windows.Forms.TextBox();
            this.regtext = new System.Windows.Forms.TextBox();
            this.departmentcomboBox = new System.Windows.Forms.ComboBox();
            this.studentListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // phonetxt
            // 
            this.phonetxt.AutoSize = true;
            this.phonetxt.Location = new System.Drawing.Point(44, 64);
            this.phonetxt.Name = "phonetxt";
            this.phonetxt.Size = new System.Drawing.Size(55, 13);
            this.phonetxt.TabIndex = 1;
            this.phonetxt.Text = "Phone No";
            // 
            // emailtxt
            // 
            this.emailtxt.AutoSize = true;
            this.emailtxt.Location = new System.Drawing.Point(44, 94);
            this.emailtxt.Name = "emailtxt";
            this.emailtxt.Size = new System.Drawing.Size(32, 13);
            this.emailtxt.TabIndex = 2;
            this.emailtxt.Text = "Email";
            // 
            // regtxt
            // 
            this.regtxt.AutoSize = true;
            this.regtxt.Location = new System.Drawing.Point(44, 124);
            this.regtxt.Name = "regtxt";
            this.regtxt.Size = new System.Drawing.Size(44, 13);
            this.regtxt.TabIndex = 3;
            this.regtxt.Text = "Reg No";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Department";
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(201, 193);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 5;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(299, 193);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Reset";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // nametext
            // 
            this.nametext.Location = new System.Drawing.Point(201, 29);
            this.nametext.Name = "nametext";
            this.nametext.Size = new System.Drawing.Size(188, 20);
            this.nametext.TabIndex = 7;
            // 
            // phonetext
            // 
            this.phonetext.Location = new System.Drawing.Point(201, 59);
            this.phonetext.Name = "phonetext";
            this.phonetext.Size = new System.Drawing.Size(188, 20);
            this.phonetext.TabIndex = 8;
            // 
            // emailtext
            // 
            this.emailtext.Location = new System.Drawing.Point(201, 92);
            this.emailtext.Name = "emailtext";
            this.emailtext.Size = new System.Drawing.Size(188, 20);
            this.emailtext.TabIndex = 9;
            // 
            // regtext
            // 
            this.regtext.Location = new System.Drawing.Point(201, 124);
            this.regtext.Name = "regtext";
            this.regtext.Size = new System.Drawing.Size(188, 20);
            this.regtext.TabIndex = 10;
            // 
            // departmentcomboBox
            // 
            this.departmentcomboBox.FormattingEnabled = true;
            this.departmentcomboBox.Location = new System.Drawing.Point(201, 153);
            this.departmentcomboBox.Name = "departmentcomboBox";
            this.departmentcomboBox.Size = new System.Drawing.Size(188, 21);
            this.departmentcomboBox.TabIndex = 11;
            // 
            // studentListView
            // 
            this.studentListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader6,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.studentListView.Location = new System.Drawing.Point(24, 250);
            this.studentListView.Name = "studentListView";
            this.studentListView.Size = new System.Drawing.Size(639, 285);
            this.studentListView.TabIndex = 12;
            this.studentListView.UseCompatibleStateImageBehavior = false;
            this.studentListView.View = System.Windows.Forms.View.Details;
            this.studentListView.DoubleClick += new System.EventHandler(this.studentListView_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Id";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 95;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Email";
            this.columnHeader3.Width = 122;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Reg No";
            this.columnHeader4.Width = 98;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Department";
            this.columnHeader5.Width = 117;
            // 
            // columnHeader6
            // 
            this.columnHeader6.DisplayIndex = 5;
            this.columnHeader6.Text = "PhoneNo";
            // 
            // Student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 560);
            this.Controls.Add(this.studentListView);
            this.Controls.Add(this.departmentcomboBox);
            this.Controls.Add(this.regtext);
            this.Controls.Add(this.emailtext);
            this.Controls.Add(this.phonetext);
            this.Controls.Add(this.nametext);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.regtxt);
            this.Controls.Add(this.emailtxt);
            this.Controls.Add(this.phonetxt);
            this.Controls.Add(this.label1);
            this.Name = "Student";
            this.Text = "Student";
            this.Load += new System.EventHandler(this.Student_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label phonetxt;
        private System.Windows.Forms.Label emailtxt;
        private System.Windows.Forms.Label regtxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox nametext;
        private System.Windows.Forms.TextBox phonetext;
        private System.Windows.Forms.TextBox emailtext;
        private System.Windows.Forms.TextBox regtext;
        private System.Windows.Forms.ComboBox departmentcomboBox;
        private System.Windows.Forms.ListView studentListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}