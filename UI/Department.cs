﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication6.BAL;
using WindowsFormsApplication6.Model;

namespace WindowsFormsApplication6.UI
{
    public partial class Department : Form
    {
        public Department()
        {
            InitializeComponent();
        }

        DepartmentModel dp1 = new DepartmentModel();
        DepartmentBAL dpbl = new DepartmentBAL();
        private void button1_Click(object sender, EventArgs e)
        {
            string message;
            dp1.Name = textBox1.Text.Trim();
            dp1.Details = textBox2.Text.Trim();
                   
            if(button1.Text == "Update")
            {
                dp1.Id = Convert.ToInt32(hiddenId.Text);
                message = dpbl.UpdateDepartment(dp1);
            }else
            {
                message = dpbl.SaveDepartment(dp1);
            }
            
            messageShow(message);
            
            populateDepartment();

        }
        public void populateDepartment()
        {
            List<DepartmentModel> deptList = dpbl.GetAllDepartment();
            listView1.Items.Clear();
            foreach (DepartmentModel dept in deptList)
            {
                ListViewItem item = new ListViewItem();
                item.Text = dept.Id.ToString();
                item.SubItems.Add(dept.Name);
                item.Tag = dept;

                listView1.Items.Add(item);
            }
        }

        private void Department_Load(object sender, EventArgs e)
        {
            populateDepartment();

        }

        public void messageShow(string message)
        {
            messagelbl.Text = message;
            messagelbl.Show();
            System.Threading.Thread.Sleep(5000);
            messagelbl.Hide();
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem selectedItem = listView1.SelectedItems[0];
            DepartmentModel dpt = selectedItem.Tag as DepartmentModel;
            textBox1.Text = dpt.Name;
            textBox2.Text = dpt.Details;
            hiddenId.Text = dpt.Id.ToString();


            button1.Text = "Update";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            hiddenId.Text = "";
            button1.Text = "Save";
        }
    }
}
