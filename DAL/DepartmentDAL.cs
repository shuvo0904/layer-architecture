﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication6.Model;
using System.Configuration;
using WindowsFormsApplication6.UI;

namespace WindowsFormsApplication6.DAL
{
    class DepartmentDAL
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["defaultConnectionString"].ToString();
        private SqlConnection connection;
        private SqlDataReader reader;
        private SqlCommand command;
        private string query;

        public DepartmentDAL()
        {
            connection = new SqlConnection(connectionString);
        }
        public int SaveDepartment(DepartmentModel dp1)
        {
            connection.Open();
            query = "INSERT INTO department(Name, Details) VALUES('"+dp1.Name + "','" + dp1.Details +"')";
            command = new SqlCommand(query, connection);
            int rowaffected = command.ExecuteNonQuery();
            connection.Close();
            return rowaffected;
        }

        public List<DepartmentModel> GetAllDepartment()
        {
            connection.Open();
            query = "SELECT * FROM department";
            command = new SqlCommand(query, connection);
            reader = command.ExecuteReader();
            List<DepartmentModel> depList = new List<DepartmentModel>();

            while (reader.Read())
            {
                DepartmentModel dp = new DepartmentModel();
                dp.Id = Convert.ToInt32(reader["Id"]);
                dp.Name = reader["Name"].ToString();
                dp.Details = reader["Details"].ToString();

                depList.Add(dp);
            }

            reader.Close();
            connection.Close();
            return depList;

        }

        public int UpdateDepartment(DepartmentModel dp1)
        {
            connection.Open();
            query = "UPDATE department SET Name='"+dp1.Name+"', Details='"+dp1.Details+"' WHERE Id='"+dp1.Id+"'";
            command = new SqlCommand(query, connection);
            int rowaffected = command.ExecuteNonQuery();
            connection.Close();
            return rowaffected;
        }
    }
}
