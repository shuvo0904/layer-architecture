﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication6.Model;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication6.DAL
{
    class StudentDAL
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["defaultConnectionString"].ToString();
        private SqlConnection connection;
        private SqlDataReader reader;
        private SqlCommand command;
        private string query;

        public StudentDAL()
        {
            connection = new SqlConnection(connectionString);
        }
        public int SaveStudent(StudentModel std)
        {
            connection.Open();
            query = "INSERT INTO studntinfo(Name, PhoneNo, Email, RegNo, Department) VALUES('" + std.Name + "','" + std.PhoneNo + "','" + std.Email + "','" + std.RegNo + "','" + std.Department + "')";
            command = new SqlCommand(query, connection);
            int rowaffected = command.ExecuteNonQuery();
            connection.Close();
            return rowaffected;
        }

        public List<StudentModel> GetAllStudent()
        {
            connection.Open();
            query = "SELECT * FROM studntinfo";
            command = new SqlCommand(query, connection);
            reader = command.ExecuteReader();
            List<StudentModel> lists = new List<StudentModel>();
            while (reader.Read())
            {
                StudentModel std = new StudentModel();
                std.Id = Convert.ToInt32(reader["Id"]);
                std.Name = reader["Name"].ToString();
                std.PhoneNo = reader["PhoneNo"].ToString();
                std.Email = reader["Email"].ToString();
                std.RegNo = reader["RegNo"].ToString();
                std.Department = Convert.ToInt32(reader["Department"]);

                lists.Add(std);
            }
            reader.Close();
            connection.Close();
            return lists;
        }
    }
}
