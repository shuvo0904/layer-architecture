﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication6.DAL;
using WindowsFormsApplication6.Model;

namespace WindowsFormsApplication6.BAL
{
    class StudentBAL
    {
        private StudentDAL stdDal = new StudentDAL();
        public string SaveStudent(StudentModel std)
        {
            int rowinfected = stdDal.SaveStudent(std);

            if(rowinfected > 0)
            {
                return "Save Successfully";
            }
            return "Save Failed";

        }

        public List<StudentModel> GetAllStudent()
        {
            List <StudentModel> stdList = stdDal.GetAllStudent();
            
            return stdList;

        }
    }
}
