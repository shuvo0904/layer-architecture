﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication6.DAL;
using WindowsFormsApplication6.Model;
using WindowsFormsApplication6.UI;

namespace WindowsFormsApplication6.BAL
{
    class DepartmentBAL
    {
        DepartmentDAL dpdl = new DepartmentDAL();
        public string SaveDepartment(DepartmentModel dp1)
        {
            int affected = dpdl.UpdateDepartment(dp1);
            if(affected > 0)
            {
                return "Save Successfully";
            }
            return "Save Failed";
        }

        public List<DepartmentModel> GetAllDepartment()
        {
            List<DepartmentModel> lists = dpdl.GetAllDepartment();
            return lists;
        }

        public string UpdateDepartment(DepartmentModel dp1)
        {
            int affected = dpdl.UpdateDepartment(dp1);
            if (affected > 0)
            {
                return "Update Successfully";
            }
            return "Update Failed";
        }
    }
}
